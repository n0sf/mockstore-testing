import { TestBed, async, fakeAsync } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { initialState } from './store/reducers/data.reducer';
import { Store } from '@ngrx/store';
import { Increment } from './store/actions/data.actions';

describe('AppComponent', () => {
    let mockStore: MockStore<{ count: number }>;
    let component: AppComponent;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AppComponent],
            providers: [
                provideMockStore({
                    initialState,
                }),
            ],
        }).compileComponents();

        mockStore = TestBed.get(Store);
    }));

    beforeEach(() => {
        const fixture = TestBed.createComponent(AppComponent);

        component = fixture.debugElement.componentInstance;
    });

    it('should create component', fakeAsync(() => {
        expect(component).toBeTruthy();
    }));

    it('should increment', fakeAsync(() => {
        spyOn(mockStore, 'dispatch');
        component.increment(1);
        expect(mockStore.dispatch).toHaveBeenCalledWith(new Increment({ count: 1 }));
    }));

    it('should select by any selector', fakeAsync(() => {
        mockStore.setState({ count: 1 });
        component.selectByPipeableSelector(1);
        expect(component.result).toBe(1);
    }));
});
